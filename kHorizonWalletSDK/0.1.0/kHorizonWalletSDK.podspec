Pod::Spec.new do |s|
  s.name             = 'kHorizonWalletSDK'
  s.version          = '0.1.0'
  s.summary          = 'kHorizonWallet SDK'
  s.swift_version    = '5.0'
  
  s.description      = "SDK for horizon wallet app"

  s.homepage         = 'https://www.targit.at'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Bojan Markovic' => 'bolom992@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/iBolom/horizon-ios-wallet-sdk.git', :tag => s.version.to_s }
  
  s.ios.deployment_target = '9.0'

  s.source_files = 'kHorizonWalletSDK/Classes/**/*'
  
  # s.resource_bundles = {
  #   'kHorizonWalletSDK' => ['kHorizonWalletSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
