#
# Be sure to run `pod lib lint kWalletSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name                  = 'kWalletSDK'
  s.version               = '0.0.3'
  s.summary               = 'Wallet SDK'
  s.swift_version         = '5.0'

  s.description           = 'SDK for horizon wallet app.'
  s.homepage              = 'https://www.targit.at'
  s.license               = { :type => 'MIT', :file => 'LICENSE' }
  s.author                = { 'Bojan Markovic' => 'bolom992@gmail.com' }
  s.source                = { :git => 'https://development.targit.at/horizon/horizon-ios-sdk', :tag => s.version.to_s }
  s.static_framework      = true
  
  s.ios.deployment_target = '11.0'

  s.source_files          = 'kWalletSDK/Classes/**/*.{swift,h,m}'
  s.public_header_files   = 'kWalletSDK/Classes/**/*.h'
  
  s.dependency              'Mantle'
  s.dependency              'CocoaLumberjack'
  s.dependency              'AFNetworking', '~> 2.6'
  s.dependency              'OpenSSL-Universal', '~> 1.0.0'
  s.dependency              'TransitionKit'
  s.dependency              'libextobjc/EXTScope'
  s.dependency              'libextobjc/EXTKeyPathCoding'
  s.dependency              'Aspects'
  s.dependency              'KVOController'
  s.dependency              'MSWeakTimer'
  
  s.pod_target_xcconfig   = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig  = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
end
